package com.sugon.test.mybatis;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

public class MybatisGenerator {

    @Test
    public void autoGenerate() {
        AutoGenerator mpg = new AutoGenerator();        // Step1：代码生成器

        GlobalConfig gc = new GlobalConfig();       // Step2：全局配置
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("luozhixiang");// 配置开发者信息（需要修改）
        gc.setFileOverride(false);
        gc.setOpen(false);
        gc.setBaseResultMap(true);
        gc.setBaseColumnList(true);
        mpg.setGlobalConfig(gc);

        DataSourceConfig dsc = new DataSourceConfig();// Step3：数据源配置
        dsc.setUrl("jdbc:mysql://10.10.14.200:3306/service_user?useUnicode=true&serverTimezone=GMT&useSSL=false&characterEncoding=utf8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("JbFnJYndsLeOsug");
        mpg.setDataSource(dsc);

        TemplateConfig templateConfig = new TemplateConfig();// 配置模板
        templateConfig.setController("");
        templateConfig.setService("");
        templateConfig.setServiceImpl("");
        mpg.setTemplate(templateConfig);

        PackageConfig pc = new PackageConfig();// Step:4：包配置
        pc.setParent("com.cloudos.cloudtest");// 配置父包名
        pc.setModuleName("mybatis");// 配置模块名（需要修改）
        mpg.setPackageInfo(pc);

        StrategyConfig strategy = new StrategyConfig();// Step5：策略配置（数据库表配置）
        strategy.setInclude("user");// 指定表名（可以同时操作多个表，使用 , 隔开）
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);

        mpg.execute();
    }
}