CREATE DATABASE  if not exists `test`;
USE `test`;

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) DEFAULT NULL,
  `city_name` varchar(10) DEFAULT NULL,
  `description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO `test`.`city`(`id`, `province_id`, `city_name`, `description`) VALUES (1, 1, 'shanghai', '上海');
INSERT INTO `test`.`city`(`id`, `province_id`, `city_name`, `description`) VALUES (2, 2, 'shenzhen', '深圳');  
