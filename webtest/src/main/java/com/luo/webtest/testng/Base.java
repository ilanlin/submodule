package com.luo.webtest.testng;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeSuite;

@SpringBootTest
public class Base extends AbstractTestNGSpringContextTests {

    @Autowired
    private Page page;

    @BeforeSuite
    public void init() throws Exception {
        springTestContextPrepareTestInstance();
        page.login();
    }

//    @AfterSuite
//    public void closeChrome() {
//        page.closeChrome();
//    }

}
