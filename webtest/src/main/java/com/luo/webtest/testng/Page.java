package com.luo.webtest.testng;

import com.luo.webtest.webux.uxpage.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Page {

    public static final String PROJECT_MENU = "//*[@id=\"/manage$Menu\"]";
    public static final String CREATE_PROJECT = "//*[@id=\"root\"]/div/section/section/div/main/div/div/div/div[2]/div/div[1]/div/div[1]/div/div/button/span";
    public static final String APP_NAME = "//span[@title='?']";
    public static final String PROJECT_ACTION = "//div[@class='cloudos-components-check-item-box-style-container']/../../div[?]";
    public static final String PROJECT_OK = "//div[@class='ant-modal-footer']/div/div[2]/button";
    public static final String MORE_SETTING = "//div[@class='cloudos-pages-dep-deploy-components-style-formContainer']//span[text()='更多配置']";
    public static final String COMPONENT_INPUT = "//label[text()='?']/../../div[2]//table/tr/td[2]//input";
    public static final String OPEN_CONFIG = "//a[@class='hover more-settings']";
    public static final String UX_EFFORT = "//span[text()='非严格模式']";
    public static final String DEPLOY = "//button[@class='button primary mr5']";
    public static final String DEPLOY_LOCATION_OPTION = "//app-project-config-ctrl-v2/div/div[1]/div[2]/table[1]/tr/td[2]/ui-select/div/div[2]//div";
    public static final String DEPLOY_LOCATION = "//app-project-config-ctrl-v2/div/div[1]/div[2]/table[1]/tr/td[2]/ui-select/div/div[1]/input";


    @Autowired
    protected Driver driver;
    @Autowired
    protected Config config;
    @Autowired
    private Login login;

    public void login() {
        login.loginUx();
    }

    public void closeChrome() {
        driver.threadWait(1000);
        driver.quit();
    }

}
