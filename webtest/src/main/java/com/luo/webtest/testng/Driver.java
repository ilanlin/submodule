package com.luo.webtest.testng;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ByIdOrName;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class Driver {

    public List<String> handles = new ArrayList<>();
    private ChromeDriver driver;
    private int waitTimeOut = 12;

    public Driver() {
        String browser = System.getenv("chrome");
        if (browser == null || browser.length() == 0 || browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver/chromedriver.exe");
            ChromeDriver driver = new ChromeDriver();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments();
            driver.manage().window().setSize(new Dimension(1920, 1080));
//            driver.manage().window().maximize();
            this.driver = driver;
        } else {
            System.setProperty("webdriver.chrome.driver", "/root/lzq/chromedriver");
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--disable-gpu");
            chromeOptions.addArguments("--disable-dev-shm-usage");
            ChromeDriver driver = new ChromeDriver(chromeOptions);
            driver.manage().window().maximize();
            this.driver = driver;
        }
    }

    public void openUrl(String url) {
        driver.get(url);
    }

    public void waitForElementAttributeToBe(String locator, String value) {
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.attributeToBe(getBy(locator), "value", value));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public void quit() {
        driver.quit();
    }

    public void waitAndClick(String locator) {
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.visibilityOfElementLocated(getBy(locator)));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
        this.getWebElementByLocator(locator).click();
    }

    public void threadWait(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void waitTime(double second) {
        try {
            Thread.sleep((long) (second * 1000));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getUrl(String url) {
        driver.get(url);
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    public String getOriginHandle() {
        return driver.getWindowHandle();
    }

    public void switchToOriginHandle(String handle) {
        driver.switchTo().window(handle);
    }

    public void getNowHandle() {
        String winHandleBefore = driver.getWindowHandle();
        for (String winHandle : driver.getWindowHandles()) {
            if (winHandle.equals(winHandleBefore)) {
                continue;
            }
            driver.switchTo().window(winHandle);
            break;
        }
    }

    public void closeWindow() {
        driver.close();
    }

    public boolean selectOptionWithText(String optionLocator, String text, double waitTSeconds) {
        waitTime(waitTSeconds);
        List<WebElement> divs = driver.findElements(getBy((optionLocator)));
//        循环获取选项的list，若与选项相同则跳出
        for (WebElement element : divs) {
            if (text.equals(element.getAttribute("innerText"))) {
                if (!element.isDisplayed()) new Actions(driver).moveToElement(element).perform();
                element.click();
                return true;
            }
        }
        return false;
    }

    public void selectOption(String selectLocator, String optionLocator) {
        click(selectLocator);
        threadWait(300);
        click(optionLocator);
    }

    public void pressEsc() {
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ESCAPE).build().perform();
    }

    public void inputUrl(String url) {
        // 输入url
        driver.get(url);
    }

    public void clear(String locator) {
        // 清空输入框
        this.getWebElementByLocator(locator).clear();
//        WebElement element = this.getWebElementByLocator(locator);
//        element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
//        element.sendKeys(Keys.DELETE);
    }

    public void input(String locator, String value) {
        this.getWebElementByLocator(locator).sendKeys(value);
    }

    public void inputById(String locator, String value) {
        driver.findElementById(locator).sendKeys(value);
    }

    public void input(String locator, String value, String attribute) {
        this.getWebElementByLocator(locator).sendKeys(value);
        this.waitForElementAttributeToBe(locator, attribute, value);
    }

    public void input(String locator, String value, Keys key) {
        this.getWebElementByLocator(locator).sendKeys(value + key);
        this.waitForElementAttributeToBe(locator, "value", value);
    }

    public void input(String locator, String value, Keys key, String notContrast) {
        this.getWebElementByLocator(locator).sendKeys(value + key);
    }

    public void clearInput(String locator, String value) {
        clear(locator);
        input(locator, value);
    }

    public void inputByPaste(String locator, String value) {
        this.setClipbordContents(value);
        Actions action = new Actions(driver);
        this.getWebElementByLocator(locator).click();
        action.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys("v").keyUp(Keys.CONTROL).keyUp(Keys.SHIFT).build().perform();
        this.waitForElementAttributeToBe(locator, "value", value);
    }

    public void click(String locator) {
        this.getWebElementByLocator(locator).click();
    }

    public void clickWithControl(String locator) {
//        this.getWebElementByLocator(locator).click();
        new Actions(driver).sendKeys(Keys.LEFT_CONTROL).click().build().perform();
//        Actions action = new Actions(driver);
//
//        action.keyDown(Keys.CONTROL).click().build().perform();
    }

    public WebElement getWebElementByLocator(String locator) {
        return driver.findElement(getBy(locator));
    }

    public By getBy(String locator) {
        /* 定位方式确认*/
        if (locator.startsWith("//") || locator.startsWith("./")) {
            return By.xpath(locator);
        } else {
            return new ByIdOrName(locator);
        }
    }

    public boolean isElementVisible(String locator) {
        /*判断元素是否可见*/
        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
            WebElement element = driver.findElement(getBy(locator));
            driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementEnabled(String locator) {
        // 元素是否启动
        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
            WebElement element = driver.findElement(getBy(locator));
            driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
            return element.isEnabled();
        } catch (Exception e) {
            return false;
        }
    }

    public String getAttribute(String locator, String attribute) {
        /*获取元素属性*/
        return this.getWebElementByLocator(locator).getAttribute(attribute);
    }

    public Object executeScript(String script) {
        JavascriptExecutor j = (JavascriptExecutor) driver;
        return j.executeScript(script);
    }

    public WebElement findParentElement(String locator) {
        // 寻找父元素对象
        return getWebElementByLocator(locator).findElement(By.xpath(".."));
    }

    public WebElement findChildElement(String locator, String childXpath) {
        // 寻找子元素对象
        return getWebElementByLocator(locator).findElement(By.xpath("." + childXpath));
    }

    public List<WebElement> findChildElements(String locator, String childXpath) {
        // 寻找子元素对象
        return getWebElementByLocator(locator).findElements(By.xpath("." + childXpath));
    }

    public void select(String locator, String label) {
        if (label != null) {
            WebElement element = getWebElementByLocator(locator);
            Select select = new Select(element);
            select.selectByVisibleText(label);
        }
    }

    public void selectByValue(String locator, String value) {
        if (value != null) {
            WebElement element = getWebElementByLocator(locator);
            Select select = new Select(element);
            select.selectByValue(value);
        }
    }

    public void selectByValue(WebElement element, String value) {
        if (value != null) {
            Select select = new Select(element);
            select.selectByValue(value);
        }
    }

    public void select(String locator, int index) {
        WebElement element = getWebElementByLocator(locator);
        Select select = new Select(element);
        select.selectByIndex(index);
    }

    public void deselectAll(String locator) {
        WebElement element = getWebElementByLocator(locator);
        Select select = new Select(element);
        select.deselectAll();
    }

    public void deselectAll(WebElement element) {
        Select select = new Select(element);
        select.deselectAll();
    }

    public List<String> getSelectedTexts(String locator) {
        WebElement element = getWebElementByLocator(locator);
        Select select = new Select(element);
        List<WebElement> elements = select.getAllSelectedOptions();
        List<String> text = new ArrayList<String>();
        for (WebElement e : elements) {
            text.add(e.getText());
        }
        return text;
    }

    public List<String> getSelectedValues(String locator) {
        WebElement element = getWebElementByLocator(locator);
        Select select = new Select(element);
        List<WebElement> elements = select.getAllSelectedOptions();
        List<String> text = new ArrayList<String>();
        for (WebElement e : elements) {
            text.add(e.getAttribute("value"));
        }
        return text;
    }

    public boolean isChecked(String locator) {
        // 是否被选中
        return this.getWebElementByLocator(locator).isSelected();
    }

    public int getElementCount(String locator) {
        // 获取定位元素个数
        if (!isElementPresent(locator)) {
            return 0;
        }
        return findElements(locator).size();
    }

    public void check(String locator) {
        if (!this.isChecked(locator)) click(locator);

    }

    public void uncheck(String locator) {
        if (this.isChecked(locator)) click(locator);
    }

    public void waitForElementVisible(String locator) {
        /*等待元素可见*/
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.visibilityOfElementLocated(getBy(locator)));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public void waitForElementPresent(String locator) {
        /*等待元素出现*/
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.presenceOfElementLocated(getBy(locator)));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public void waitForElementNotVisible(String locator) {
        /*等待元素不可见*/
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.invisibilityOfElementLocated(getBy(locator)));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public void waitForElementAttributeToBe(String locator, String attribute, String value) {
        /*等待元素属性与预期值相等*/
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.attributeToBe(getBy(locator), attribute, value));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public void waitForElementToBeClickable(String locator) {
        /*等待元素可点击*/
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.elementToBeClickable(getBy(locator)));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public boolean isElementPresent(String locator) {
        /*元素是否立即出现*/
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        boolean exists = driver.findElements(getBy(locator)).size() != 0;
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
        return exists;
    }

    public boolean isElementPresent(WebElement element, String locator) {
        /*元素在选择区域内是否出现*/
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        boolean exists = element.findElements(getBy(locator)).size() != 0;
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
        return exists;
    }

    public boolean isElementPresent(String locator, int second) {
        /*元素在一段时间内是否出现*/
        driver.manage().timeouts().implicitlyWait(second, TimeUnit.SECONDS);
        boolean exists = driver.findElements(getBy(locator)).size() != 0;
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
        return exists;
    }

    public boolean isChildElementPresent(WebElement element, String locator) {
        /*元素在选择区域内是否出现*/
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        boolean exists = element.findElements(getBy(locator)).size() != 0;
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
        return exists;
    }

    public void waitForTextPresent(String text) {
        /*等待文字出现*/
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"), text));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public void waitForTextNotPresent(String text) {
        /*等待文字消失*/
        (new WebDriverWait(driver, waitTimeOut)).until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"), text)));
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
    }

    public WebElement findElementByIndex(String locator, int index) {
        return findElements(locator).get(index);
    }

    public WebElement findLastElement(String locator) {
        // 定位到多个元素中的最后一个
        List<WebElement> elements = findElements(locator);
        return elements.get(elements.size() - 1);
    }

    public List<WebElement> findElements(String locator) {
        return driver.findElements(getBy(locator));
    }

    public String getText(String locator) {
        return this.getWebElementByLocator(locator).getText();
    }

    public void back() {
        driver.navigate().back();
    }

    public List<WebElement> getWebElementsByLocator(String locator) {
        return driver.findElements(getBy(locator));
    }

    public void doubleClick(String locator) {
        Actions actions = new Actions(driver);
        actions.doubleClick(getWebElementByLocator(locator)).build().perform();
    }

    public void close() {
        driver.close();
    }

    public void moveToElement(String locator, int xOffset, int yOffset) {
        //将鼠标移动到元素指定位置
        Actions actions = new Actions(driver);
        actions.moveToElement(getWebElementByLocator(locator), xOffset, yOffset).build().perform();
    }

    public void moveToElementAndClick(String locator, int xOffset, int yOffset) {
        //将鼠标移动到元素指定位置并点击
        Actions actions = new Actions(driver);
        actions.moveToElement(getWebElementByLocator(locator), xOffset, yOffset).click().build().perform();
    }

    public void dragAndDrop(String sourceLocator, String targetLocator, int xOffset, int yOffset) {
        //将源元素拖拉到目标元素指定位置
        Actions actions = new Actions(driver);
        actions.clickAndHold(getWebElementByLocator(sourceLocator)).moveToElement(getWebElementByLocator(targetLocator), xOffset, yOffset).release(getWebElementByLocator(sourceLocator)).build().perform();
    }

    public List<String> selectMultiOption(String selectLocator, String optionLocator, List<String> options) {
        // 多选点击
        if (options == null) return null;
        moveToElementAndClick(selectLocator, 0, 0);
        this.threadWait(300);
        // 获取所有的需要选择的li标签
        ArrayList<String> liTitle = new ArrayList<>();
        List<WebElement> lis = findElements(optionLocator);
        // 清空已选中
        for (WebElement li : lis) {
            if (li.getAttribute("class").contains("selected")) {
                liTitle.add(li.getAttribute("innerText"));
                li.click();
                threadWait(300);
            }
        }
        // 选择新选项
        for (WebElement li : lis) {
            if (options.contains(li.getAttribute("innerText"))) {
                li.click();
                threadWait(300);
            }
        }
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ESCAPE).build().perform();
        threadWait(300);
        return liTitle;
    }

    public void selectMultiOption(WebElement parentWebElement, String childSelectXpath, String optionLocator, List<String> options) {
        // 多选点击
        if (options == null) return;
        Actions actions = new Actions(driver);
        actions.moveToElement(parentWebElement.findElement(By.xpath("." + childSelectXpath)), 0, 0).click().build().perform();
        this.threadWait(300);
        // 获取所有的需要选择的li标签
        List<WebElement> lis = findElements(optionLocator);
        // 清空已选中
        for (WebElement li : lis) {
            if (li.getAttribute("class").contains("selected")) {
                li.click();
                threadWait(300);
            }
        }
        // 选择新选项
        for (WebElement li : lis) {
            if (options.contains(li.getAttribute("innerText"))) {
                li.click();
                threadWait(300);
            }
        }
        actions.sendKeys(Keys.ESCAPE).build().perform();
        threadWait(300);
    }

    public void selectOption(WebElement parentWebElement, String childSelectXpath, String optionLocator, String option) {
        // 单选点击
        if (option == null) return;
        parentWebElement.findElement(By.xpath("." + childSelectXpath)).click();
        threadWait(300);
        // 获取所有的需要选择的li标签
        List<WebElement> lis = findElements(optionLocator);
        // 循环获取选项的list，若与选项相同则跳出
        for (WebElement li : lis) {
            if (option.equals(li.getAttribute("innerText"))) {
                li.click();
                break;
            }
        }
        threadWait(300);
    }

    private void setClipbordContents(String contents) {
        StringSelection stringSelection = new StringSelection(contents);
        // 系统剪贴板
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }

    public void uploadFile(String inputLocator, File file) {
        WebElement inputTag = getWebElementByLocator(inputLocator);
        inputTag.sendKeys(file.getAbsolutePath());
    }

    public void switchToIframe(int frameIndex) {
        driver.switchTo().frame(frameIndex);
    }

    public void jumpoutFrame() {
        driver.switchTo().defaultContent();
    }

    public void OpenAnotherHandle(String url) {
        String currentHandle = driver.getWindowHandle();
        handles.add(currentHandle);
//        String js = "window.open('" + url + "')";
//        http:
////webux.pkg427beta4.cloud2go.cn/ ')";

//        JavascriptExecutor oJavaScriptExecutor = (JavascriptExecutor) driver;
//        oJavaScriptExecutor.executeScript("window.open('" + URL + "')");
//        System.out.println("there are " + driver.getWindowHandles().size

        driver.executeScript("window.open('" + url + "')");
        for (String windowHandle : driver.getWindowHandles()) {
            if (!handles.toString().contains(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }
    }
}
