package com.luo.webtest.testng;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "login")
public class Config {
    public String loginUrl;
    public String user;
    public String password;
    public String type;
    public String factoryUrl;
    public String gz;
    public String sz1;
    public String sz2;
    public String codeSource;
    public String gitProject;
    public String gitBranch;
    public String codeStack;
    public String subModule;
    public String netPort;
    public String deployLocation;
    public String image;
    public String cmd;
    public String logPath;
    public String healthCheck;
    public String networkPort;
    public String domainSuffix;
    public String tag1;
    public String tag2;

}


