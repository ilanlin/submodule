package com.luo.webtest.tools;

public class LocatorParser {

    public static String parse(String source, int index) {
        source = source.replaceAll("\\?", String.valueOf(index));
        return source;
    }

    public static String parse(String source, String... strs) {
        if (strs == null || strs.length == 0 || strs[0] == null) {
            return null;
        }
        for (String str : strs) {
            source = source.replaceFirst("\\?", str);
        }
        return source;
    }

    public static String parse(String source, int... ints) {
        if (ints == null || ints.length == 0) {
            return null;
        }
        for (int i : ints) {
            source = source.replaceFirst("\\?", String.valueOf(i));
        }
        return source;
    }

}
