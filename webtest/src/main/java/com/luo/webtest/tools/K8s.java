package com.luo.webtest.tools;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Namespace;
import io.kubernetes.client.openapi.models.V1NamespaceList;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.KubeConfig;

import java.io.FileReader;
import java.io.IOException;

public class K8s {
    public static void main(String[] args) throws ApiException, IOException, ApiException {
        String kubeConfigPath = "D:\\project\\prod-luo\\submodule\\webtest\\src\\main\\resources\\k8s-config\\14.33-ux-config";
        ApiClient client = ClientBuilder.kubeconfig(KubeConfig.loadKubeConfig(new FileReader(kubeConfigPath))).build();
        Configuration.setDefaultApiClient(client);
        CoreV1Api v1Api = new CoreV1Api();

        V1NamespaceList list = v1Api.listNamespace(null, null, null, null, null, null, null,
                null, null);

        for (V1Namespace item : list.getItems()) {
            System.out.println(item);
        }
    }
}