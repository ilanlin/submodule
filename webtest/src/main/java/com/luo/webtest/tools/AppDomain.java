package com.luo.webtest.tools;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class AppDomain {
    public List<String> domainList = new ArrayList<>();
    public Map<String, String> domainMap = new HashMap<>();

    public void addDomainList(String value) {
        domainList.add(value);
        log.debug("-------domainList,发布应用使用了域名     " + value);
    }

    public void addDomainMap(String name, String value) {
        domainMap.put(name, value);
        log.debug("-------domainMap,发布" + name + "应用使用了域名     " + value);
    }
}
