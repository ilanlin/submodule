package com.luo.webtest.tools;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiRobotSendRequest;
import com.taobao.api.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Slf4j
@Component
public class DingDingTalk {
    private static final String digndingUrl = "https://oapi.dingtalk.com/robot/send?access_token=dacc50f3d278fe6c70b8f7b8ab6f83c3e8216d04ac96a841648bfc04ba8b88c2";
    private static final String secret = "SEC1abb0774e4ec64e4308272e10dc74a4e2fa1629317b8ae58066200bd3f7f5011";
    private final Long timestamp = System.currentTimeMillis();

    public String getSign() throws Exception {
        String stringToSign = timestamp + "\n" + secret;
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256"));
        byte[] signData = mac.doFinal(stringToSign.getBytes(StandardCharsets.UTF_8));
        return URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
    }

    public boolean sendDingDing(String msg) {
        String url = null;
        try {
            url = digndingUrl + "&timestamp=" + timestamp + "&sign=" + getSign();
        } catch (Exception e) {
            log.error("获取钉钉告警url出错\n");
            e.printStackTrace();
        }
        DingTalkClient client = new DefaultDingTalkClient(url);
        OapiRobotSendRequest request = new OapiRobotSendRequest();
        request.setMsgtype("text");
        OapiRobotSendRequest.Text text = new OapiRobotSendRequest.Text();
        text.setContent(msg);
        request.setText(text);
        try {
            return client.execute(request).isSuccess();
        } catch (ApiException e) {
            log.error("钉钉client.execute推送消息出错\n");
            e.printStackTrace();
        }
        return false;
    }

}