package com.luo.webtest.tools;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

@Component
public class SSHLinux {
    public static final String podInNamespace = "kc get po -A|grep ide-|sort -n -k 6";
    public static final String podInNamespaceSort = "kc get po -A|grep ide-|awk '{print $1 \" \" $2}' |xargs  -L1  kubectl describe po -n |grep -i  image:|awk -F/ '{print $NF}'|grep -v git-alpine";
    public static final String getNs = "kc get po -A|grep ide-|grep |awk '{print $1 \" \" $2}'|xargs -i kubectl describe po -n {}";
    public static final String ux = "10.10.14.33";
    public String namespace;

    public String exeCmd(String host, int port, String user, String password, String command) throws JSchException, IOException {
        JSch jsch = new JSch();
        Session session = jsch.getSession(user, host, port);
        session.setConfig("StrictHostKeyChecking", "no");
        session.setPassword(password);
        session.connect();

        ChannelExec channelExec = (ChannelExec) session.openChannel("exec");
        InputStream in = channelExec.getInputStream();
        channelExec.setCommand(command);
        channelExec.setErrStream(System.err);
        channelExec.connect();
        String result = IOUtils.toString(in, "UTF-8");
        channelExec.disconnect();
        session.disconnect();
        return result;
    }

    public void strToFile(String str) throws IOException {
        FileWriter fw = new FileWriter("src/main/resources/pod.txt", false);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(str);
        bw.close();
        fw.close();
    }

    //    @Test
    public void a() throws IOException, JSchException {
//        namespace = exeCmd(ux, 22, "root", "password", podInNamespaceSort);
        String str = exeCmd(ux, 22, "root", "password", podInNamespaceSort);


    }

//
//    @org.junit.Test
//    public void ssh() throws IOException, JSchException {
//        String str = exeCmd(ux, 22, "root", "password", podInNamespaceSort);
//        strToFile("\n\n\n\n" + TimeUtil.formatTime() + "\n" + str);
//    }
}