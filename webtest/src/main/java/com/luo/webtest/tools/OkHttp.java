package com.luo.webtest.tools;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class OkHttp {
    private static final MediaType mediaType = MediaType.parse("application/json; charset=utf-8");

    public static String get(String url) {
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().url(url).build();
        Response response;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            log.error("Invoker fail" + url);
            return null;
        }
        if (response.code() != 200) return null;
        if (response.body() != null) {
            try {
                return response.body().string();
            } catch (IOException e) {
                log.error("responseBody 异常");
            }
        }
        return null;
    }

    public static Response getResponse(String url) {
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().url(url).build();
        Response response;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            log.error("Invoker fail" + url);
            return null;
        }
        return response;
    }

    public static String post(String url, JSONObject json) {
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(mediaType, json.toString());
        Response response;
        Request request = new Request.Builder().url(url).post(body).build();
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            log.error("Invoker fail" + url);
            return null;
        }
        if (response.body() != null) {
            try {
                return response.body().string();
            } catch (IOException e) {
                log.error("responseBody异常");
            }
        }
        return null;
    }


}