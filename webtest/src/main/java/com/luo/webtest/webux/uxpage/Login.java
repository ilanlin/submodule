package com.luo.webtest.webux.uxpage;

import com.luo.webtest.testng.Page;
import org.springframework.stereotype.Component;

@Component
public class Login extends Page {

    //登录页面
    public static final String UX_LOGIN = "//button[@type='submit']";
    public static final String UX_USER_NAME = "//*[@id=\"account\"]";
    public static final String UX_USER_PASSWORD = "//*[@id=\"password\"]";

    public void loginUx() {
        driver.openUrl(config.getLoginUrl());
        driver.waitForElementVisible(UX_USER_NAME);
        driver.input(UX_USER_NAME, config.getUser());
        driver.input(UX_USER_PASSWORD, config.getPassword());
        driver.click(UX_LOGIN);
        driver.waitTime(1.5);
    }
}
