package com.luo.webtest.webux.uxtest;

import com.luo.webtest.testng.Base;
import com.luo.webtest.tools.TimeUtil;
import com.luo.webtest.webux.uxpage.BlankProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;


public class TestCode extends Base {

    @Autowired
    private BlankProject blankProject;

    @Test
    public void testCodeComponent() {
        blankProject.addComponent("", "code" + TimeUtil.formatTime());
    }

}
