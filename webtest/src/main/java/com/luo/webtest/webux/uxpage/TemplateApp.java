package com.luo.webtest.webux.uxpage;

import com.luo.webtest.testng.Page;
import com.luo.webtest.tools.LocatorParser;
import com.luo.webtest.tools.RandomValue;
import org.springframework.stereotype.Component;

@Component
public class TemplateApp extends Page {

    public String createTemplateApp(String name, String component, String deployLocation) {
        driver.OpenAnotherHandle(config.getLoginUrl());
        driver.waitAndClick(PROJECT_MENU);
        driver.waitAndClick(CREATE_PROJECT);
        driver.waitAndClick(LocatorParser.parse(APP_NAME, name));
        driver.waitAndClick(LocatorParser.parse(PROJECT_ACTION, 3));
        driver.waitTime(0.3);
        driver.click(PROJECT_OK);
        driver.waitAndClick(MORE_SETTING);
        driver.waitTime(4);
        driver.switchToIframe(0);
        driver.waitAndClick(DEPLOY_LOCATION);
        driver.selectOptionWithText(DEPLOY_LOCATION_OPTION, deployLocation, 0);
        String domainValue = component + RandomValue.getRandomString(8);
        driver.clearInput(LocatorParser.parse(COMPONENT_INPUT, component), domainValue);
        driver.waitTime(0.5);
        if ((driver.isElementVisible(LocatorParser.parse(COMPONENT_INPUT, "mysql"))))
            driver.clearInput(LocatorParser.parse(COMPONENT_INPUT, "mysql"), "password");
        if (!driver.isElementVisible(UX_EFFORT)) {
            driver.click(OPEN_CONFIG);
            driver.waitAndClick(UX_EFFORT);
        }
        driver.click(DEPLOY);
        driver.waitTime(1);
        return domainValue;
    }
}
