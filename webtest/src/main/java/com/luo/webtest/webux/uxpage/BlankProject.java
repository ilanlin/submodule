package com.luo.webtest.webux.uxpage;

import com.luo.webtest.testng.Page;
import com.luo.webtest.tools.LocatorParser;
import org.springframework.stereotype.Component;

@Component
public class BlankProject extends Page {
    private static final String BLANK_PROJECT_NAME = "//div[@class='ant-modal-body']/form/div[1]/div[2]//input";
    private static final String PROJECT_NAME1 = "//div[@id='rcDialogTitle0']";

    private static final String BLANK_PROJECT_OK = "//div[@class='ant-modal-footer']/div/div[2]/button";
    private static final String FIRST_PROJECT = "//div[@class='ant-row']/div[1]//h4";
    private static final String PROJECT_ACT = "//div[@class='ant-card-head-title' and text()='项目动态']";
    private static final String BLUEPRINT = "//div[@class='ant-pro-top-nav-header-main ']/div[1]/ul/li[text()='架构图']";
    private static final String CREATE_BLUE = "//button[@class='ant-btn ant-btn-primary' and text='+ 创建']";

    public void addComponent(String tempName, String projectName) {
        driver.waitAndClick(PROJECT_MENU);
        driver.waitAndClick(CREATE_PROJECT);
        driver.waitAndClick(LocatorParser.parse(APP_NAME, tempName));
        driver.waitForElementVisible(PROJECT_NAME1);
        driver.clearInput(BLANK_PROJECT_NAME, projectName);
        driver.click(BLANK_PROJECT_OK);
        driver.waitForElementVisible(FIRST_PROJECT);
        if (!driver.getText(FIRST_PROJECT).equals(projectName)) return;
        driver.click(FIRST_PROJECT);
        driver.waitForElementVisible(BLUEPRINT);
        driver.click(CREATE_BLUE);


//        driver.waitAndClick(LocatorParser.parse(PROJECT_ACTION, 3));
//        driver.waitTime(0.3);
//        driver.click(PROJECT_OK);
//        driver.waitAndClick(MORE_SETTING);
//        driver.waitTime(4);
//        driver.switchToIframe(0);
//        driver.waitForElementVisible(BASE_CONFIG);
//        String domainValue = component + RandomValue.getRandomString(8);
//        driver.clearInput(LocatorParser.parse(COMPONENT_INPUT, component), domainValue);
//        driver.waitTime(0.5);
//        if ((driver.isElementVisible(LocatorParser.parse(COMPONENT_INPUT, "mysql"))))
//            driver.clearInput(LocatorParser.parse(COMPONENT_INPUT, "mysql"), "password");
//        if (!driver.isElementVisible(UX_EFFORT)) {
//            driver.click(OPEN_CONFIG);
//            driver.waitAndClick(UX_EFFORT);
//        }
//        return domainValue;
    }

}

