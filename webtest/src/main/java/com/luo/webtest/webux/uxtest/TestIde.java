package com.luo.webtest.webux.uxtest;

import com.jcraft.jsch.JSchException;
import com.luo.webtest.testng.Base;
import com.luo.webtest.tools.SSHLinux;
import com.luo.webtest.tools.TimeUtil;
import com.luo.webtest.webux.uxpage.Ide;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.io.IOException;

@Slf4j
public class TestIde extends Base {
    public static final String podInNamespace = "kc get po -A|grep ide-|sort -n -k 6";
    public static final String ux = "10.10.14.33";
    public static final String podInNamespaceSort = "kc get po -A|grep ide-|awk '{print $1 \" \" $2}' |xargs  -L1  kubectl describe po -n |grep -i  image:|awk -F/ '{print $NF}'|grep -v git-alpine";

    @Autowired
    private Ide ide;
    @Autowired
    private SSHLinux sshLinux;

    @Test
    public void testIde() throws IOException, JSchException {
        ide.createcode();
        logger.info("创建全部代码库成功");
        ide.openIde();
        logger.info("打开全部ide成功");

        String str = sshLinux.exeCmd(ux, 22, "root", "password", podInNamespaceSort);
        sshLinux.strToFile("\n\n\n\n" + TimeUtil.formatTime() + "\n" + str);

    }

}
