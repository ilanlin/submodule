package com.luo.webtest.webux.interfaces;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.luo.webtest.testng.Config;
import com.luo.webtest.tools.AppDomain;
import com.luo.webtest.tools.OkHttp;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class InvokerApp {

    @Autowired
    private AppDomain appDomain;
    @Autowired
    private Config config;

    public boolean invokerJavaApp() {
        JSONObject json;
        String url = "http://" + appDomain.domainMap.get("java") + config.getDomainSuffix() + "cloudtogo";
        Response response = OkHttp.getResponse(url);
        if (response != null && response.body() != null && response.code() == 200) {
            String result;
            try {
                result = response.body().string();
            } catch (Exception e) {
                log.error("调java应用接口,获取结果出错 " + url);
                return false;
            }
            json = (JSONObject) JSON.parse(result);
            if (json.getString("version").equals("5.5.45")) {
                log.info("访问java-normal应用,结果与预期一致 " + url);
                return true;
            }
        }
        log.error("访问java应用,结果不正确 " + url);
        return false;
    }

    public boolean invokerPythonApp() {
        String url = "http://" + appDomain.domainMap.get("Python") + config.getDomainSuffix();
        Response response = OkHttp.getResponse(url);
        if (response != null && response.body() != null && response.code() == 200) {
            String result;
            try {
                result = response.body().string();
            } catch (Exception e) {
                log.error("调python应用接口,获取结果出错 " + url);
                return false;
            }
            if (result.contains("已连接到MySQL") && result.contains("python") && result.contains("flask")) {
                log.info("访问python应用,结果与预期一致 " + url);
                return true;
            }
        }
        log.error("访问python应用,结果不正确 " + url);
        return false;
    }

    public boolean invokerGoApp() {
        String url = "http://" + appDomain.domainMap.get("golang") + config.getDomainSuffix();
        Response response = OkHttp.getResponse(url);
        if (response != null && response.body() != null && response.code() == 200) {
            String result;
            try {
                result = response.body().string();
            } catch (Exception e) {
                log.error("调go应用接口,获取结果出错 " + url);
                return false;
            }
            if (result.contains("已连接到MySQL") && result.contains("golang") && result.contains("beego")) {
                log.info("访问go应用,结果与预期一致 " + url);
                return true;
            }
        }
        log.error("访问go应用,结果不正确 " + url);
        return false;
    }

    public boolean invokerDotnetApp() {
        String url = "http://" + appDomain.domainMap.get("netcore") + config.getDomainSuffix();
        Response response = OkHttp.getResponse(url);
        if (response != null && response.body() != null && response.code() == 200) {
            String result;
            try {
                result = response.body().string();
            } catch (Exception e) {
                log.error("调dotnet应用接口,获取结果出错 " + url);
                return false;
            }
            if (result.contains("Connected") && result.contains("php") && result.contains("laravel")) {
                log.info("访问dotnet应用,结果与预期一致 " + url);
                return true;
            }
        }
        log.error("访问dotnet应用,结果不正确 " + url);
        return false;
    }

    public boolean invokerPythonHApp() {
        String url = "http://" + appDomain.domainMap.get("PythonH") + config.getDomainSuffix();
        Response response = OkHttp.getResponse(url);
        if (response != null && response.body() != null && response.code() == 200) {
            String result;
            try {
                result = response.body().string();
            } catch (Exception e) {
                log.error("调python hello world应用接口,获取结果出错 " + url);
                return false;
            }
            if (result.contains("本页面被访问了") && result.contains("word 拼写错误")) {
                log.info("访问python hello world应用,结果与预期一致 " + url);
                return true;
            }
        }
        log.error("访问python hello world应用,结果不正确 " + url);
        return false;
    }

    public boolean invokerNodejsApp() {
        String url = "http://" + appDomain.domainMap.get("nodejs") + config.getDomainSuffix();
        Response response = OkHttp.getResponse(url);
        if (response != null && response.body() != null && response.code() == 200) {
            String result;
            try {
                result = response.body().string();
            } catch (Exception e) {
                log.error("调nodejs应用接口,获取结果出错 " + url);
                return false;
            }
            if (result.contains("本页面被访问了") && result.contains("Node.js项目")) {
                log.info("访问nodejs应用,结果与预期一致 " + url);
                return true;
            }
        }
        log.error("访问nodejs应用,结果不正确 " + url);
        return false;
    }

    public boolean invokerImageApp() {
        String url = "http://" + appDomain.domainMap.get("image") + config.getDomainSuffix();
        Response response = OkHttp.getResponse(url);
        if (response != null && response.body() != null && response.code() == 200) {
            String result;
            try {
                result = response.body().string();
            } catch (Exception e) {
                log.error("调submoduleImage应用接口,获取结果出错 " + url);
                return false;
            }
            if (result.contains("module1")) {
                log.info("访问submoduleImage应用,结果与预期一致 " + url);
                return true;
            }
        }
        log.error("访问submoduleImage应用,结果不正确 " + url);
        return false;
    }
}
