package com.luo.webtest.webux.uxpage;

import com.luo.webtest.testng.Page;
import com.luo.webtest.tools.LocatorParser;
import com.luo.webtest.tools.TimeUtil;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Ide extends Page {
    public static final String CODE_MENU = "//ul[@id='/dev$Menu']/li[1]";//代码库菜单
    public static final String CREATE_CODE = "//div[@class='ant-card-head-title']//span[text()='+ 创建']";
    public static final String CODE_NAME = "//input[@placeholder='请输入代码库名称，由字母、数字、或_开头，字母、数字、_、-或.组成']";
    public static final String CODESTACK_SEL = "//div[@class='ant-select-selector']";
    public static final String CODESTACK_OPTION = "//div[@role='listbox']/../div[2]/div/div/div";
    public static final String CREATE_OK = "//button[@type='submit']";
    public static final String STACK_LIST = "//div[@class='ant-row']/div";
    public static final String STACK_LIST_ul = "//div[@class='ant-row']/div[?]//ul[@class='ant-card-actions']/li[1]";
    public static final String IDE = "//div[@class='ant-row']/div";

    public List<String> stackNames = new ArrayList<>();

    public void codingEnv() {
        driver.waitAndClick(CODE_MENU);
        driver.click(CREATE_CODE);
        driver.click(CODESTACK_SEL);
        List<WebElement> divs = driver.findElements(CODESTACK_OPTION);
        for (WebElement ele : divs) {
            String name = ele.getAttribute("innerText");
            stackNames.add(name);
        }
        driver.waitAndClick(CODE_MENU);
        driver.waitForElementVisible(CREATE_CODE);
    }

    public void createcode() {
        codingEnv();
        for (String stackName : stackNames) {
            createOneCode(stackName);
        }

    }

    public void createOneCode(String stackName) {
        driver.click(CREATE_CODE);
        driver.input(CODE_NAME, stackName.replace(".", "") + TimeUtil.formatTime().replace(":", ".").replace(" ", ""));
        driver.click(CODESTACK_SEL);
        driver.selectOptionWithText(CODESTACK_OPTION, stackName, 0.3);
        driver.click(CREATE_OK);
        driver.waitTime(1);
    }

    public void openIde() {
        driver.waitAndClick(CODE_MENU);
        driver.waitForElementVisible(CREATE_CODE);
        for (int i = 0; i < stackNames.size(); i++) {
            driver.OpenAnotherHandle(config.getLoginUrl());
            driver.waitAndClick(CODE_MENU);
            driver.waitForElementVisible(CREATE_CODE);
            driver.click(LocatorParser.parse(STACK_LIST_ul, i));
            driver.waitTime(5);
        }
    }

}

