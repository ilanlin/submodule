package com.luo.webtest.webux.uxtest;

import com.luo.webtest.testng.Base;
import com.luo.webtest.tools.AppDomain;
import com.luo.webtest.webux.uxpage.TemplateApp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

@Slf4j
public class TestTemplateApp extends Base {
    @Autowired
    private TemplateApp templateApp;
    @Autowired
    private AppDomain appDomain;

    @Test
    private void testJava() {
        String javaDomain = templateApp.createTemplateApp("Java + MySql", "java", "DC1");
        this.appDomain.addDomainMap("java", javaDomain);
        log.info("发布了ux-java模板 ");
    }

    @Test
    private void testPython() {
        String pythonDomain = templateApp.createTemplateApp("Python + MySql", "Python", "DC1");
        this.appDomain.addDomainMap("Python", pythonDomain);
        log.info("发布了ux-python模板 ");
    }

    @Test
    private void testGo() {
        String goDomain = templateApp.createTemplateApp("Golang + MySql", "golang", "DC1");
        this.appDomain.addDomainMap("golang", goDomain);
        log.info("发布了ux-golang模板 ");
    }

    @Test
    private void testDotnet() {
        String dotnetDomain = templateApp.createTemplateApp(".NETCore + MySql", "netcore", "DC1");
        this.appDomain.addDomainMap("netcore", dotnetDomain);
        log.info("发布了ux-NETCore模板 ");
    }

    @Test
    private void testPythonH() {
        String pythonDomain = templateApp.createTemplateApp("Python Hello World", "python", "DC1");
        this.appDomain.addDomainMap("PythonH", pythonDomain);
        log.info("发布了ux-Python Hello World模板 ");
    }

    @Test
    private void testNodejs() {
        String nodejsDomain = templateApp.createTemplateApp("Nodejs Hello World", "nodejs", "DC1");
        this.appDomain.addDomainMap("nodejs", nodejsDomain);
        log.info("发布了ux-nodejs模板 ");
    }


}
