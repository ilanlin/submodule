package com.luo.request.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
public class RequestController {
    private final String url = "http://response:8082/";
//    private final String url = "http://localhost:8082";

    @GetMapping("/header")
    private String sendHeader(@RequestHeader("key") String key) {
        HttpHeaders header = new HttpHeaders();
        header.add("key", key);
        RestTemplate rest = new RestTemplate(new OkHttp3ClientHttpRequestFactory());
        ResponseEntity<String> result = rest.exchange(url + "/header", HttpMethod.GET, new HttpEntity<>(header), String.class);
        log.info(result.toString());
        return result.getBody();
    }

    @GetMapping(value = "/")
    private String root() {
        RestTemplate rest = new RestTemplate(new OkHttp3ClientHttpRequestFactory());
        long startTime = System.currentTimeMillis();
        String result = rest.getForEntity(url, String.class).getBody();
        String resp = "请求用时:  " + (System.currentTimeMillis() - startTime) + " ms  去请求response服务,结果:  " + result;
        log.info(resp);
        return resp;
    }

    @GetMapping(value = "/request")
    private String request() {
        RestTemplate rest = new RestTemplate(new OkHttp3ClientHttpRequestFactory());
        long startTime = System.currentTimeMillis();
        String result = rest.getForEntity(url + "/response", String.class).getBody();
        String resp = "请求用时:  " + (System.currentTimeMillis() - startTime) + " ms  去请求 /response 接口,结果:  " + result;
        log.info(resp);
        return resp;
    }

    @GetMapping(value = "/request1")
    private String request1() {
        RestTemplate rest = new RestTemplate(new OkHttp3ClientHttpRequestFactory());
        long startTime = System.currentTimeMillis();
        String result = rest.getForEntity(url + "/response1", String.class).getBody();
        String resp = "请求用时:  " + (System.currentTimeMillis() - startTime) + " ms  去请求 response1 接口,结果:  " + result;
        log.info(resp);
        return resp;
    }

    @GetMapping(value = "/requestWrong")
    private String requestWrong() {
        RestTemplate rest = new RestTemplate(new OkHttp3ClientHttpRequestFactory());
        long startTime = System.currentTimeMillis();
        String result = rest.getForEntity(url + "/responseWrong", String.class).getBody();
        String resp = "请求用时:  " + (System.currentTimeMillis() - startTime) + " ms  去请求 responseWrong 接口,结果:  " + result;
        log.info(resp);
        return resp;
    }

}
