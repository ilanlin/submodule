package com.luo.module1.controller;

import com.luo.module2.domain.City;
import com.luo.module2.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class Module1Controller {
    @Autowired(required = false)
    private CityService cityService;

    @GetMapping("/")
    public String root() {
        log.info("访问了 module1 /");
        return "jar包 module1模块测试1";
    }

    @GetMapping("/health")
    public String healthcheck() {
        log.info("访问了module1 /health");
        return "health OK";
    }

    @GetMapping("/city")
    public City city() {
        log.info("访问了module1 /city");
        return cityService.findCityByName("shanghai");
    }

    @GetMapping("/healthz")
    public String healthz() {
        log.info("访问了module1 /healthz");
        return "health OK";
    }

}
