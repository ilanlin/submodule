package com.luo.response.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ResponseController {

    @GetMapping(value = "/")
    public String root() {
//        try {
//            Thread.sleep(2500);
//        } catch (InterruptedException e) {
//            log.info(e.toString());
//        }
        log.info("响应-8082端口");
        return "响应-8082口";
    }

    @GetMapping("/header")
    public String getHeader(@RequestHeader(value = "key") String key) {
        String mess = "响应8082-header的key为key,value为" + key;
        log.info(mess);
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            log.info(String.valueOf(e));
//        }
        return mess;
    }

    @GetMapping("/response")
    public String response() {
        log.info("响应8082-/response接口");
        return "响应8082-/response接口";
    }

    @GetMapping("/response1")
    public String response1() {
        log.info("响应8082-/response1接口");
        return "响应8082-/response1接口";
    }

    @GetMapping("/responseWrong")
    public String responseWrong() {
        String result = "默认字符串";
        int rd = Math.random() == 0.5 ? 1 : 0;
        if (rd == 0) {
            log.info("响应8082-/responseWrong接口");
            result = String.valueOf(1 / 0);
        } else if (rd == 1) {
            log.info("响应8082-/responseCorrect接口");
            result = "响应8082-/responseCorrect接口";
        }
        return result;
    }

}
