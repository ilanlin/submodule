package com.luo.war;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.luo.war.dao")
public class WarApplication {

    public static void main(String[] args) {
        SpringApplication.run(WarApplication.class, args);
    }
}
