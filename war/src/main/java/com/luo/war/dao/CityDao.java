package com.luo.war.dao;

import com.luo.war.domain.City;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface CityDao {

    City findByName(@Param("cityName") String cityName);
}
