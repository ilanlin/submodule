package com.luo.war.service;

import com.luo.war.domain.City;

public interface CityService {

    City findCityByName(String cityName);
}
