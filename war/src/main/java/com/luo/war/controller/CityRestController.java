package com.luo.war.controller;

import com.luo.war.domain.City;
import com.luo.war.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CityRestController {

    @Autowired
    private CityService cityService;

    @GetMapping(value = "/city")
    public City findOneCity() {
        return cityService.findCityByName("beijing");
    }

    @GetMapping(value = "/one")
    public String findOne() {
        return "shanghai";
    }

}
